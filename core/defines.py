type_profile = (
    ('Alumno','Alumno'),
    ('Docente','Docente'),
    ('Administrador','Administrador'),
    )
alternativas = (
    ('A','A'),
    ('B','B'),
    ('C','C'),
    ('D','D'),
    ('E','E'),
    )
semestre = (
    ('Primer Semestre','Primer Semestre'),
    ('Segundo Semestre','Segundo Semestre'),
    )
año = (
    ('2018','2018'),
    ('2019','2019'),
    ('2020','2020'),
    ('2021','2021'),
    ('2022','2022'),
    )
AÑO_DEFAULT = '2018'
ALTERNATIVA_DEFAULT = 'A'
SEMESTRE_DEFAULT = 'Primer Semestre'
TIPO_DEFAULT = 'Alumno'