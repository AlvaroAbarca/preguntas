from django import forms
from .models import Asignatura, Perfil, Respuesta, Pregunta, Survey, Evaluacion, Respuesta_Alumno


class AsignaturaForm(forms.ModelForm):
    class Meta:
        model = Asignatura
        fields = ['nombre', 'codigo', 'semestre','año']
        

class ProfileForm(forms.ModelForm):
    class Meta:
        model = Perfil
        fields = ['name', 'last_name', 'rut', 'birth_day', 'email', 'tipo']


class RespuestaForm(forms.ModelForm):
    class Meta:
        model = Respuesta
        fields = ['name', 'respuesta', 'correcto']


class PreguntaForm(forms.ModelForm):
    class Meta:
        model = Pregunta
        fields = ['enunciado', 'puntaje', 'respuesta']


class SurveyForm(forms.ModelForm):
    class Meta:
        model = Survey
        fields = ['name', 'description', 'asignatura', 'pregunta']


class EvaluacionForm(forms.ModelForm):
    class Meta:
        model = Evaluacion
        fields = ['puntaje', 'survey', 'profile']


class Respuesta_AlumnoForm(forms.ModelForm):
    class Meta:
        model = Respuesta_Alumno
        fields = ['puntaje', 'evaluacion', 'respuesta']


