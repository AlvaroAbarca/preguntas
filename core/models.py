from django.urls import reverse
from django.db.models import *
from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth import get_user_model
from django.contrib.auth import models as auth_models
from django.db import models as models
from core.defines import *
from datetime import date


class Perfil(models.Model):

    # Fields
    name = CharField(max_length=140)
    last_name = CharField(max_length=140)
    rut = CharField(max_length=15, help_text = "12345678-9", blank = True)
    birth_day = DateField(default =date.today)
    email = EmailField(blank = True)
    tipo = CharField(max_length=10,choices = type_profile, default=TIPO_DEFAULT)


    class Meta:
        ordering = ('-pk',)
        verbose_name = 'perfil'
        verbose_name_plural = 'perfiles'

    def __str__(self):
        return self.name

    def __unicode__(self):
        return u'%s' % self.pk

    def get_absolute_url(self):
        return reverse('core_profile_detail', args=(self.pk,))


    def get_update_url(self):
        return reverse('core_profile_update', args=(self.pk,))


class Asignatura(models.Model):

    # Fields
    nombre = CharField(max_length=140)
    codigo = CharField(max_length=140)
    semestre = CharField(max_length=20, choices = semestre, default=SEMESTRE_DEFAULT)
    año = models.CharField(max_length=4, choices= año, default=AÑO_DEFAULT)


    class Meta:
        ordering = ('-pk',)
        verbose_name = 'asignatura'
        verbose_name_plural = 'asignaturas'

    def __str__(self):
        return self.nombre

    def __unicode__(self):
        return u'%s' % self.pk

    def get_absolute_url(self):
        return reverse('core_asignatura_detail', args=(self.pk,))


    def get_update_url(self):
        return reverse('core_asignatura_update', args=(self.pk,))



class Respuesta(models.Model):

    # Fields
    name = CharField(max_length=140)
    respuesta = CharField(max_length=1, choices = alternativas,default= ALTERNATIVA_DEFAULT)
    correcto = BooleanField(default=False)


    class Meta:
        ordering = ('-pk',)
        verbose_name = 'respuesta'
        verbose_name_plural = 'respuestas'

    def __str__(self):
        return self.name

    def __unicode__(self):
        return u'%s' % self.pk

    def get_absolute_url(self):
        return reverse('core_respuesta_detail', args=(self.pk,))


    def get_update_url(self):
        return reverse('core_respuesta_update', args=(self.pk,))


class Pregunta(models.Model):

    # Fields
    enunciado = TextField(default = '')
    puntaje = PositiveIntegerField(default = 0,help_text="Puntaje de la Pregunta")

    # Relationship Fields
    respuesta = ManyToManyField(Respuesta)
    asignatura = ForeignKey(
    	Asignatura,
    	on_delete=models.CASCADE,
    	)

    class Meta:
        ordering = ('-pk',)
        verbose_name = 'pregunta'
        verbose_name_plural = 'preguntas'

    def __str__(self):
        return self.enunciado

    def __unicode__(self):
        return u'%s' % self.pk

    def get_absolute_url(self):
        return reverse('core_pregunta_detail', args=(self.pk,))


    def get_update_url(self):
        return reverse('core_pregunta_update', args=(self.pk,))

    def get_respuestas(self):
    	return self.respuesta


class Survey(models.Model):

    # Fields
    name = CharField(max_length=140)
    description = TextField(blank = True, null=True)
    created = models.DateField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now_add=True, editable=False)

    # Relationship Fields
    asignatura = ForeignKey(
    	Asignatura,
    	on_delete=models.CASCADE, 
    	related_name='asignaturas', 
    	related_query_name='asignatura'
    	)
    pregunta = ManyToManyField(
    	Pregunta,
    	related_name='preguntas',
    	related_query_name='cuestionario'
    	)


    class Meta:
        ordering = ('-created',)
        verbose_name = 'cuestionario'
        verbose_name_plural = 'cuestionarios'


    def __str__(self):
        return self.name

    def __unicode__(self):
        return u'%s' % self.pk

    def get_absolute_url(self):
        return reverse('core_survey_detail', args=(self.pk,))


    def get_update_url(self):
        return reverse('core_survey_update', args=(self.pk,))


#class Contenido(models.model):

	#Fields
#	contenido = CharField(max_length = 20)


class Evaluacion(models.Model):

    # Fields
    puntaje = PositiveIntegerField(default =0)

    # Relationship Fields
    survey = ForeignKey(
    	Survey,
    	on_delete=models.CASCADE,
    	related_name='cuestionarios',
    	related_query_name='cuestionario'
    	)
    profile = ForeignKey(
    	Perfil,
    	on_delete=models.CASCADE,
    	related_name='perfiles',
    	related_query_name='perfil'
    	)

    class Meta:
        ordering = ('-pk',)
        verbose_name = 'evaluación'
        verbose_name_plural = 'evaluaciones'

    def __unicode__(self):
        return u'%s' % self.pk

    def get_absolute_url(self):
        return reverse('core_evaluacion_detail', args=(self.pk,))


    def get_update_url(self):
        return reverse('core_evaluacion_update', args=(self.pk,))


class Respuesta_Alumno(models.Model):

    # Fields
    puntaje = PositiveIntegerField(default = 0)

    # Relationship Fields
    evaluacion = ForeignKey(
    	Evaluacion,
    	on_delete=models.CASCADE,
    	)
    respuesta = ForeignKey(
    	Respuesta,
    	on_delete=models.CASCADE,
    	)

    class Meta:
        ordering = ('-pk',)
        verbose_name = 'Respuesta_Alumno'
        verbose_name_plural = 'Respuesta_Alumnos'

    def __unicode__(self):
        return u'%s' % self.pk

    def get_absolute_url(self):
        return reverse('core_respuesta_alumno_detail', args=(self.pk,))


    def get_update_url(self):
        return reverse('core_respuesta_alumno_update', args=(self.pk,))


