from django.urls import path, include

from . import views

urlpatterns = (
    path('', views.index, name='index'),
)

urlpatterns += (
    # urls for Asignatura
    path('core/asignatura/', views.AsignaturaListView.as_view(), name='core_asignatura_list'),
    path('core/asignatura/create/', views.AsignaturaCreateView.as_view(), name='core_asignatura_create'),
    path('core/asignatura/detail/<int:pk>/', views.AsignaturaDetailView.as_view(), name='core_asignatura_detail'),
    path('core/asignatura/update/<int:pk>/', views.AsignaturaUpdateView.as_view(), name='core_asignatura_update'),
)

urlpatterns += (
    # urls for Profile
    path('core/profile/', views.ProfileListView.as_view(), name='core_profile_list'),
    path('core/profile/create/', views.ProfileCreateView.as_view(), name='core_profile_create'),
    path('core/profile/detail/<int:pk>/', views.ProfileDetailView.as_view(), name='core_profile_detail'),
    path('core/profile/update/<int:pk>/', views.ProfileUpdateView.as_view(), name='core_profile_update'),
)

urlpatterns += (
    # urls for Respuesta
    path('core/respuesta/', views.RespuestaListView.as_view(), name='core_respuesta_list'),
    path('core/respuesta/create/', views.RespuestaCreateView.as_view(), name='core_respuesta_create'),
    path('core/respuesta/detail/<int:pk>/', views.RespuestaDetailView.as_view(), name='core_respuesta_detail'),
    path('core/respuesta/update/<int:pk>/', views.RespuestaUpdateView.as_view(), name='core_respuesta_update'),
)

urlpatterns += (
    # urls for Pregunta
    path('core/pregunta/', views.PreguntaListView.as_view(), name='core_pregunta_list'),
    path('core/pregunta/create/', views.AgregarPregunta, name='core_pregunta_create'),
    path('core/pregunta/detail/<int:pk>/', views.PreguntaDetailView.as_view(), name='core_pregunta_detail'),
    path('core/pregunta/update/<int:pk>/', views.PreguntaUpdateView.as_view(), name='core_pregunta_update'),
)

urlpatterns += (
    # urls for Survey
    path('core/survey/', views.SurveyListView.as_view(), name='core_survey_list'),
    path('core/survey/create/', views.SurveyCreateView.as_view(), name='core_survey_create'),
    path('core/survey/detail/<int:pk>/', views.SurveyDetailView.as_view(), name='core_survey_detail'),
    path('core/survey/update/<int:pk>/', views.SurveyUpdateView.as_view(), name='core_survey_update'),
)

urlpatterns += (
    # urls for Evaluacion
    path('core/evaluacion/', views.EvaluacionListView.as_view(), name='core_evaluacion_list'),
    path('core/evaluacion/create/', views.EvaluacionCreateView.as_view(), name='core_evaluacion_create'),
    path('core/evaluacion/detail/<int:pk>/', views.EvaluacionDetailView.as_view(), name='core_evaluacion_detail'),
    path('core/evaluacion/update/<int:pk>/', views.EvaluacionUpdateView.as_view(), name='core_evaluacion_update'),
)

urlpatterns += (
    # urls for Respuesta_Alumno
    path('core/respuesta_alumno/', views.Respuesta_AlumnoListView.as_view(), name='core_respuesta_alumno_list'),
    path('core/respuesta_alumno/create/', views.Respuesta_AlumnoCreateView.as_view(), name='core_respuesta_alumno_create'),
    path('core/respuesta_alumno/detail/<int:pk>/', views.Respuesta_AlumnoDetailView.as_view(), name='core_respuesta_alumno_detail'),
    path('core/respuesta_alumno/update/<int:pk>/', views.Respuesta_AlumnoUpdateView.as_view(), name='core_respuesta_alumno_update'),
)

