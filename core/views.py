from django.views.generic import DetailView, ListView, UpdateView, CreateView
from .models import Asignatura, Perfil, Respuesta, Pregunta, Survey, Evaluacion, Respuesta_Alumno
from .forms import AsignaturaForm, ProfileForm, RespuestaForm, PreguntaForm, SurveyForm, EvaluacionForm, Respuesta_AlumnoForm
from django.shortcuts import render, redirect
from django.http import JsonResponse,HttpResponseRedirect

def index(request):
    data = {}
    template_name = 'index.html'
    return render(request,template_name,data)


class AsignaturaListView(ListView):
    model = Asignatura


class AsignaturaCreateView(CreateView):
    model = Asignatura
    form_class = AsignaturaForm


class AsignaturaDetailView(DetailView):
    model = Asignatura


class AsignaturaUpdateView(UpdateView):
    model = Asignatura
    form_class = AsignaturaForm


class ProfileListView(ListView):
    model = Perfil


class ProfileCreateView(CreateView):
    model = Perfil
    form_class = ProfileForm


class ProfileDetailView(DetailView):
    model = Perfil


class ProfileUpdateView(UpdateView):
    model = Perfil
    form_class = ProfileForm


class RespuestaListView(ListView):
    model = Respuesta


class RespuestaCreateView(CreateView):
    model = Respuesta
    form_class = RespuestaForm


class RespuestaDetailView(DetailView):
    model = Respuesta


class RespuestaUpdateView(UpdateView):
    model = Respuesta
    form_class = RespuestaForm


class PreguntaListView(ListView):
    model = Pregunta


class PreguntaCreateView(CreateView):
    model = Pregunta
    form_class = PreguntaForm

def AgregarPregunta(request):
    data = {}
    var1 = False
    var2 = False
    var3 = False
    var4 = False 
    var5 = False
    if request.method == "POST":
        asig = request.POST["asignatura"]
        enunciado = request.POST["enunciado"]
        puntaje = request.POST["puntaje"]
        A = request.POST["respuestaA"]
        B = request.POST["respuestaB"]
        C = request.POST["respuestaC"]
        D = request.POST["respuestaD"]
        E = request.POST["respuestaE"]
        correcta =  request.POST["correcta"]
        asignatura = Asignatura.objects.get(pk=int(asig))
        if (correcta == 'A'):
            var1 = True
        elif(correcta == 'B') :
            var2 = True
        elif(correcta == 'C'):
            var3 = True
        elif(correcta == 'D'):
            var4 = True
        elif(correcta == 'E'):
            var5 = True
        respuesta1 = Respuesta.objects.create(
            name = A,
            respuesta = str('A'),
            correcto = var1,
        )
        respuesta2 = Respuesta.objects.create(
            name = B,
            respuesta = str('B'),
            correcto = var2,
        )
        respuesta3 = Respuesta.objects.create(
            name = C,
            respuesta = str('C'),
            correcto = var3,
        )
        respuesta4 = Respuesta.objects.create(
            name = D,
            respuesta = str('D'),
            correcto = var4,
        )
        respuesta5 = Respuesta.objects.create(
            name = E,
            respuesta = str('E'),
            correcto = var5,
        )
        respuesta1.save()
        respuesta2.save()
        respuesta3.save()
        respuesta4.save()
        respuesta5.save()
        preguntaAdd = Pregunta.objects.create(
            enunciado = enunciado, 
            puntaje = puntaje,
            asignatura = asignatura,
        )
        preguntaAdd.respuesta.add(respuesta1,respuesta2,respuesta3,respuesta4,respuesta5)
        preguntaAdd.save()
        data = {
            "enunciado": enunciado,
            "asignatura": puntaje
        }
        
        return JsonResponse(data)
    else:
        data['asignaturas'] = Asignatura.objects.all()
    template_name = 'core/pregunta_form.html'
    return render(request,template_name,data)


class PreguntaDetailView(DetailView):
    model = Pregunta


class PreguntaUpdateView(UpdateView):
    model = Pregunta
    form_class = PreguntaForm


class SurveyListView(ListView):
    model = Survey


class SurveyCreateView(CreateView):
    model = Survey
    form_class = SurveyForm


class SurveyDetailView(DetailView):
    model = Survey


class SurveyUpdateView(UpdateView):
    model = Survey
    form_class = SurveyForm


class EvaluacionListView(ListView):
    model = Evaluacion


class EvaluacionCreateView(CreateView):
    model = Evaluacion
    form_class = EvaluacionForm


class EvaluacionDetailView(DetailView):
    model = Evaluacion


class EvaluacionUpdateView(UpdateView):
    model = Evaluacion
    form_class = EvaluacionForm


class Respuesta_AlumnoListView(ListView):
    model = Respuesta_Alumno


class Respuesta_AlumnoCreateView(CreateView):
    model = Respuesta_Alumno
    form_class = Respuesta_AlumnoForm


class Respuesta_AlumnoDetailView(DetailView):
    model = Respuesta_Alumno


class Respuesta_AlumnoUpdateView(UpdateView):
    model = Respuesta_Alumno
    form_class = Respuesta_AlumnoForm

