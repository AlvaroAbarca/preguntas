from django.contrib import admin
from django import forms
from .models import Asignatura, Perfil, Respuesta, Pregunta, Survey, Evaluacion, Respuesta_Alumno

class AsignaturaAdminForm(forms.ModelForm):

    class Meta:
        model = Asignatura
        fields = '__all__'


class AsignaturaAdmin(admin.ModelAdmin):
    form = AsignaturaAdminForm
    list_display = ['nombre', 'codigo', 'semestre','año']

admin.site.register(Asignatura, AsignaturaAdmin)


class ProfileAdminForm(forms.ModelForm):

    class Meta:
        model = Perfil
        fields = '__all__'


class ProfileAdmin(admin.ModelAdmin):
    form = ProfileAdminForm
    list_display = ['name', 'last_name', 'rut', 'birth_day', 'email', 'tipo']

admin.site.register(Perfil, ProfileAdmin)


class RespuestaAdminForm(forms.ModelForm):

    class Meta:
        model = Respuesta
        fields = '__all__'


class RespuestaAdmin(admin.ModelAdmin):
    form = RespuestaAdminForm
    list_display = ['name', 'respuesta', 'correcto']

admin.site.register(Respuesta, RespuestaAdmin)


class PreguntaAdminForm(forms.ModelForm):

    class Meta:
        model = Pregunta
        fields = '__all__'


class PreguntaAdmin(admin.ModelAdmin):
    form = PreguntaAdminForm
    list_display = ['enunciado', 'puntaje']

admin.site.register(Pregunta, PreguntaAdmin)


class SurveyAdminForm(forms.ModelForm):

    class Meta:
        model = Survey
        fields = '__all__'


class SurveyAdmin(admin.ModelAdmin):
    form = SurveyAdminForm
    list_display = ['name', 'description', 'created', 'last_updated']

admin.site.register(Survey, SurveyAdmin)


class EvaluacionAdminForm(forms.ModelForm):

    class Meta:
        model = Evaluacion
        fields = '__all__'


class EvaluacionAdmin(admin.ModelAdmin):
    form = EvaluacionAdminForm
    list_display = ['puntaje']

admin.site.register(Evaluacion, EvaluacionAdmin)


class Respuesta_AlumnoAdminForm(forms.ModelForm):

    class Meta:
        model = Respuesta_Alumno
        fields = '__all__'


class Respuesta_AlumnoAdmin(admin.ModelAdmin):
    form = Respuesta_AlumnoAdminForm
    list_display = ['puntaje']

admin.site.register(Respuesta_Alumno, Respuesta_AlumnoAdmin)


